﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class tutorialselect : MonoBehaviour 
{

	public Image selectionImage; //= new Image();
	public List<Sprite> Itemlist = new List<Sprite>();
	private int itemSpot = 0;

	public void RightSelection(){

		if (itemSpot < Itemlist.Count - 1) {
			itemSpot++;
			selectionImage.sprite = Itemlist [itemSpot];
		}
	}

	public void LeftSelection(){
		if (itemSpot > 0) {
			itemSpot--;
			selectionImage.sprite = Itemlist [itemSpot];
		}
	}

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
