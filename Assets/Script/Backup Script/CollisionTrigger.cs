﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//untuk mengatur collider dari sebuah objek
public class CollisionTrigger : MonoBehaviour {
    //private BoxCollider2D playerCollider;
    //private BoxCollider2D playerCollider1;
    [SerializeField]
    private BoxCollider2D platformCollider;
    [SerializeField]
    private BoxCollider2D platformTrigger;
	// untuk mengambil collider dari objek player
	void Start () {
        //playerCollider = GameObject.Find("Player").GetComponent<BoxCollider2D>();
        //playerCollider1 = GameObject.Find("Enemy").GetComponent<BoxCollider2D>();
        Physics2D.IgnoreCollision(platformCollider, platformTrigger, true);
    }
	
	// untuk mengatur supaya player dapat berdiri di objek dan dapat melewati objek tersebut tanpa tersangkut
	void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player" || other.gameObject.tag == "Enemy")
        {
            Physics2D.IgnoreCollision(platformCollider, other, true);
        }
        //if (other.gameObject.name == "Enemy")
        //{
        //    Physics2D.IgnoreCollision(platformCollider, playerCollider1, true);
        //}
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player" || other.gameObject.tag == "Enemy")
        {
            Physics2D.IgnoreCollision(platformCollider, other, false);
        }
        //if (other.gameObject.name == "Enemy")
        //{
        //    Physics2D.IgnoreCollision(platformCollider, playerCollider1, false);
        //}
    }
}
