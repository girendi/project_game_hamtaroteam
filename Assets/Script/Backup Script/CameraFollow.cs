﻿using UnityEngine;
using System.Collections;

//untuk membuat kamera selalu mengikuti player
public class CameraFollow : MonoBehaviour {

    [SerializeField]
    private float xMax;

    [SerializeField]
    private float yMax;

    [SerializeField]
    private float xMin;

    [SerializeField]
    private float yMin;

    private Transform target;

    void Start ()
    {
        //Untuk mengambil posisi perubahan player
        target = GameObject.Find("Player").transform;
    }

    void LateUpdate()
    {
        //untuk mengatur posisi nilai x dan y maksimum maupun minumum.
        transform.position = new Vector3(Mathf.Clamp(target.position.x, xMin, xMax), Mathf.Clamp(target.position.y, yMin, yMax), transform.position.z);
    }
}
