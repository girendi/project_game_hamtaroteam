﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{
    [SerializeField]
    protected Transform BulletPos;

    [SerializeField]
    protected float speed;

    protected bool facingRight;

    [SerializeField]
    private GameObject bulletPrefab;

    [SerializeField]
    protected int health;

    [SerializeField]
    private EdgeCollider2D swordCollider;

    [SerializeField]
    private List<string> damageSource;

    public abstract bool IsDead { get; }

    public bool Attack { get; set; }

    public bool TakingDemage { get; set; }

    public Animator MyAnimator { get; private set; }

    public EdgeCollider2D SwordCollider
    {
        get
        {
            return swordCollider;
        }
    }

    // Use this for initialization
    public virtual void Start ()
    {

        facingRight = true;

        MyAnimator = gameObject.GetComponent<Animator>();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public abstract IEnumerator TakeDemage();

    public abstract void Death();

    public void ChangeDirection()
    {

        facingRight = !facingRight;

        transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);

    }

    public virtual void ShootBullet(int value)
    {
        if (facingRight)
        {
            GameObject tmp = (GameObject)Instantiate(bulletPrefab, BulletPos.position, Quaternion.Euler(new Vector3(0, 180, 0)));
            tmp.GetComponent<Shoot>().Initialize(Vector2.right);
        }
        else
        {
            GameObject tmp = (GameObject)Instantiate(bulletPrefab, BulletPos.position, Quaternion.Euler(new Vector3(0, 0, 0)));
            tmp.GetComponent<Shoot>().Initialize(Vector2.left);
        }
    }

    public void MeleeAttack()
    {
        SwordCollider.enabled = true;
    }

    public virtual void OnTriggerEnter2D(Collider2D other)
    {
        if (damageSource.Contains(other.tag))
        {
            StartCoroutine(TakeDemage());
        }
    }
}
