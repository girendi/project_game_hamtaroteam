﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StarManager : MonoBehaviour {

	private static StarManager instance;

	[SerializeField]
	private GameObject starPrefab;

	[SerializeField]
	private Text starText;

	private int collectedStars;

	public int CollectedStars {
		get {
			return collectedStars;
		}
		set {
			starText.text = value.ToString ();
			collectedStars = value;
		}
	}

	public GameObject StarPrefab{
		get{
			return starPrefab;
		}
	}

	public static StarManager Instance{
		get{
			if (instance == null) {
				instance = FindObjectOfType<StarManager> ();
			}
			return instance;
		}
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
