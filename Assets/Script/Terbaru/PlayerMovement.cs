﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public delegate void DeadEventHandler();

public class PlayerMovement : Character {

    private static PlayerMovement instance;

    public event DeadEventHandler Dead;

    public static PlayerMovement Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<PlayerMovement>();
            }
            return instance;
        }

    }

    [SerializeField]
    private Transform[] groundPoints;

    [SerializeField]
    private float groundRadius;
    
    [SerializeField]
    private LayerMask whatIsGround;


    [SerializeField]
    private bool AirControl;

    [SerializeField]
    private float JumpForce;

    private bool immortal = false;

    private SpriteRenderer spriteRenderer;

    [SerializeField]
    private float immortalTime;

    private float direction;

    private bool move;

    private float btnHorizontal;

	[SerializeField]
	private int sum;

	[SerializeField]
	private GameObject door;

	[SerializeField]
	private int live;

	[SerializeField]
	private GameObject gameOver;

	[SerializeField]
	private Text blood;

	[SerializeField]
	private Text life;

	[SerializeField]
	private int tempHealth;

    public Rigidbody2D MyRigidbody { get; set; }

    public bool Slide { get; set; }

    public bool Jump { get; set; }

    public bool OnGround { get; set; }

    public override bool IsDead
    {
        get
        {
            if (health <=0)
            {
                OnDead();
            }
            return health <= 0;
        }
    }

    public bool IsFalling
    {
        get
        {
            return MyRigidbody.velocity.y < 0;
        }
    }

    private Vector2 startPos;

    // Use this for initialization
    public override void Start ()
    {
        base.Start();

        startPos = transform.position;

        spriteRenderer = GetComponent<SpriteRenderer>();

        MyRigidbody = GetComponent<Rigidbody2D>();

		door.SetActive (false);

		gameOver.gameObject.SetActive (false);
        
	}

    void Update()
    {
		blood.text = Convert.ToString (health);
		life.text = Convert.ToString (live);
		if (live != 0) {
			if (!TakingDemage && !IsDead) {
				if (transform.position.y <= 0) {
					MyRigidbody.velocity = Vector2.zero;
					transform.position = startPos;
					Death ();
				}

				HandleInput ();
			}
		} else {
			gameObject.SetActive (false);
			gameOver.gameObject.SetActive (true);
		}
        
    }
	  
	// Update is called once per frame
	void FixedUpdate () {
        if (!TakingDemage && !IsDead)
        {
            float horizontal = Input.GetAxis("Horizontal");

            OnGround = IsGrounded();

            if (move)
            {
                this.btnHorizontal = Mathf.Lerp(btnHorizontal, direction, Time.deltaTime * 2);
                HandleMovement(btnHorizontal);
                Flip(direction);
            }
            else
            {
                HandleMovement(horizontal);

                Flip(horizontal);
            }

            HandleLayers();

            ResetValue();
        }
        
    }

    public void OnDead()
    {
        if (Dead != null)
        {
            Dead();
        }
    }

    private void HandleMovement(float horizontal)
    {
        if (IsFalling)
        {
            gameObject.layer = 10;
            MyAnimator.SetBool("land", true);
        }
        if (!Attack && !Slide && (OnGround || AirControl))
        {
            MyRigidbody.velocity = new Vector2(horizontal * speed, MyRigidbody.velocity.y);
        }
        if (Jump && MyRigidbody.velocity.y == 0)
        {
            MyRigidbody.AddForce(new Vector2(0, JumpForce));
        }
        MyAnimator.SetFloat("speed", Mathf.Abs(horizontal));
    }


    private void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            MyAnimator.SetTrigger("attack");
        }
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            MyAnimator.SetTrigger("slide");
        }
        if (Input.GetKeyDown(KeyCode.Space) && !IsFalling)
        {
            //Debug.Log("jump");
            MyAnimator.SetTrigger("jump");
            Jump = true;
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            MyAnimator.SetTrigger("shoot");
        }
    }

    private void Flip(float horizontal)
    {
        if (horizontal > 0 && !facingRight || horizontal < 0 && facingRight)
        {
            ChangeDirection();
        }
    }

    private bool IsGrounded()
    {
        if (MyRigidbody.velocity.y <= 0)
        {
            foreach (Transform point in groundPoints)
            {
                Collider2D[] colliders = Physics2D.OverlapCircleAll(point.position, groundRadius, whatIsGround);

                for (int i = 0; i < colliders.Length; i++)
                {
                    if (colliders[i].gameObject != gameObject)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void HandleLayers()
    {
        if (!OnGround)
        {
            MyAnimator.SetLayerWeight(1, 1);
        }
        else
        {
            MyAnimator.SetLayerWeight(1, 0);
        }
    }

    public override void ShootBullet(int value)
    {
        if (!OnGround && value == 1 || OnGround && value == 0)
        {
            base.ShootBullet(value);
        }
    }

    private IEnumerator IndicateImmortal()
    {
        while (immortal)
        {
            spriteRenderer.enabled = false;
            yield return new WaitForSeconds(.1f);
            spriteRenderer.enabled = true;
            yield return new WaitForSeconds(.1f);
        }
    }

    public override IEnumerator TakeDemage()
    {
        if (!immortal)
        {
            health -= 10;
            if (!IsDead)
            {
                MyAnimator.SetTrigger("demage");

                immortal = true;
                StartCoroutine(IndicateImmortal());
                yield return new WaitForSeconds(immortalTime);

                immortal = false;
            }
            else
            {
                MyAnimator.SetLayerWeight(1, 0);
                MyAnimator.SetTrigger("die");
            }
        }
    }

    public override void Death()
    {
        MyRigidbody.velocity = Vector2.zero;
        MyAnimator.SetTrigger("idle");
		health = tempHealth;
        transform.position = startPos;
		live--;
		//Debug.Log (live);
    }

    public void BtnJump()
    {
        MyAnimator.SetTrigger("jump");
        Jump = true;
    }

    public void BtnAttack()
    {
        MyAnimator.SetTrigger("attack");
    }

    public void BtnSlide()
    {
        MyAnimator.SetTrigger("slide");
    }

    public void BtnShoot()
    {
        MyAnimator.SetTrigger("shoot");
    }

    public void BtnMove(float direction)
    {
        this.direction = direction;
        this.move = true;
    }

    public void BtnStopMove()
    {
        this.direction = 0;
        this.btnHorizontal = 0;
        this.move = false;
    }

    public void ResetValue()
    {
        Jump = false;
    }

	private void OnCollisionEnter2D(Collision2D other)
	{

		if (other.gameObject.tag == "Star") {
			StarManager.Instance.CollectedStars++;
			GameManager.Instance.CollectedCoins += 10;
			int count = StarManager.Instance.CollectedStars;
			if (count >= sum) {
				//Debug.Log("Finish");
				door.SetActive (true);
			}
			Destroy (other.gameObject);

		}
	}
}

