﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColletCoin : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player") {
			GameManager.Instance.CollectedCoins++;
			Destroy (gameObject);
		}
	}
}
