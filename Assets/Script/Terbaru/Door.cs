﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {

	[SerializeField]
	private GameObject UIWin;

	public void Start (){
		UIWin.gameObject.SetActive (false);
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player")
		{
			UIWin.gameObject.SetActive (true);
		}
	}

}
