﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedState : IEnemyState
{
    private Enemy enemy;

    private float hurtTimer;
    private float hurtCoolDown = 5;
    private bool canHurt = true;

    public void Enter(Enemy enemy)
    {
        this.enemy = enemy;
    }

    public void Execute()
    {
        HurtSpirit();

        if (enemy.InMeleeRange)
        {
            enemy.ChangeState(new MeleeState());
        }
        else if (enemy.Target != null)
        {
            enemy.Move();
        }
        else
        {
            enemy.ChangeState(new IdleState());
        }
    }

    public void Exit()
    {
        
    }

    public void OnTriggerEnter(Collider2D other)
    {
        
    }

    private void HurtSpirit()
    {
        hurtTimer += Time.deltaTime;

        if (hurtTimer >= hurtCoolDown)
        {
            canHurt = true;
            hurtTimer = 0;
        }

        if (canHurt)
        {
            canHurt = false;
            enemy.MyAnimator.SetTrigger("shoot");
        }
    }
}
